from main_lib import make_str_lower
import os
import pytest

def test_make_str_lower():
    assert make_str_lower("Hello") == "hello"
    assert make_str_lower("hEllo") == "hello"
    assert make_str_lower("HeLlo") == "hello"
    assert make_str_lower("HelLo") == "hello"
    assert make_str_lower("HellO") == "hello"
    assert make_str_lower("HeLlO WorLd") == "hello world"

def test_env_var_exists():
    assert "SUPER_IMPORTANT_VAR" in os.environ
    assert "OTHER_IMPORTANT_VAR" in os.environ